CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

With Smart Content Segments, groups of conditions, called Segments, can be created, edited and managed from a single interface. Segments can then be referenced by Smart Blocks without setting up the same individual sets of conditions for each Smart Block.

Sample segments:

  * A “Spanish on the Go” Segment might target visitors on mobile devices, with their browser language set to Spanish.

  * A “Desktop Mac” Segment might target visitors who are not on mobile devices and are viewing the site on a computer running Mac OS.


REQUIREMENTS
------------

This module requires the following modules:

  * Smart Content (https://www.drupal.org/project/smart_content)


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. Visit: https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for further information.


CONFIGURATION
-------------

  * Add a Smart Segment

    - Navigate to Structure » Smart segment in the administration toolbar. This page will list all Smart Segments that have been added, if none have been created, you will see the message "There are no Smart Segement entities yet."

    - Click the "Add Smart segment" button to add a new Smart Segment.

    - Create all of the conditions you'll need within the Smart Segment and click "Save."

  *  Once a Smart Segment has been saved, it will be available to use in the conditions section of any Smart Content Variation Set.


MAINTAINERS
-----------

Current maintainers:

  * Michael Lander (michaellander); Primary Developer  - https://www.drupal.org/u/michaellander

  * Gurwinder Antal (gantal); Developer - https://www.drupal.org/u/gantal

  * Nick Switzer (switzern); Documentation, QA, Project Management -  https://www.drupal.org/u/switzern


This project has been sponsored by:

  * Elevated Third
    Empowering B2B marketing ecosystems with strategic thinking, top-notch user experience design and world-class Drupal development.