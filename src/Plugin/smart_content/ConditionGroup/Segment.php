<?php

namespace Drupal\smart_content\Plugin\smart_content\ConditionGroup;

/**
 * Provides a group for classifying smart segments.
 *
 * @SmartConditionGroup(
 *   id = "segment",
 *   label = @Translation("Segment")
 * )
 */
class Segment {}
