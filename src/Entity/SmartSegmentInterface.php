<?php

namespace Drupal\smart_content_segments\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Smart segment entities.
 */
interface SmartSegmentInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
